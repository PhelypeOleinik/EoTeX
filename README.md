# The EoTeX document class

This is the official repository of the EoTeX document class.

This is the README of the first official release.

The files in this repository are:

- `eotex.dtx` --- The source file for the class. You probably don't want this file unless you know what you're doing;
- `eotex.cls` --- The class file. This is the file you must have with your main `.tex` file;
- `eotex.pdf` --- The (incomplete) documentation (in Portuguese only, for now) of the class.

TODO:

- Finish documentation;
- Fix known and unknown bugs;
- Make english version of the documentation;

